package com.androsor.example;

import java.util.Scanner;

public class Example {

    private static final String INPUT_MESSAGE = "Please enter your name:";
    private static final String OUTPUT_MESSAGE = "Hello, %s! Приятно познакомиться!";
    private static final Scanner INSTANCE = new Scanner(System.in);

    public static void main(String[] args) {
        print();
    }

    private static void print() {
        System.out.println(INPUT_MESSAGE);
        String userName = getUserName();
        System.out.printf(OUTPUT_MESSAGE, userName);
    }

    public static String getUserName() {
        return getInstance().nextLine();
    }

    public static Scanner getInstance() {
        return INSTANCE;
    }
}